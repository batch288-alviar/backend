	const express = require("express");
	const mongoose = require("mongoose");

	const usersRoutes = require("./routes/usersRoutes.js")

	const coursesRoutes = require("./routes/coursesRoutes.js")
	
	// It will allow our backend application to be available to our frontend application
	// It will also allow us to control app's cross origin resource sharing settings
	const cors = require("cors");

	const port = 4001;

	const app = express();

	// MongoDB connection
	// Establish the connection between the DB and the application or server.
	// The name of the database should be :"CourseBookingAPI"

	mongoose.connect("mongodb+srv://admin:admin@batch288alviar.iizabv5.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {useNewUrlParser: true,
		useUnifiedTopology: true
	});

	let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Error, Can't connect to the database."));

	db.once("open", () => console.log("Connected to the cloud database!"));

	// Middlewares
	app.use(express.json());
	app.use(express.urlencoded({extended:true}));

	// Reminder that we are going to use this for the sake of the bootcamp
	app.use(cors());

	// add the routing of the routes from the usersRouts 
	app.use("/users", usersRoutes);

	app.use("/courses", coursesRoutes);

	if(require.main === module){
		app.listen(process.env.PORT || 4000, () => {
		    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
		});
	}

	module.exports = {app,mongoose};