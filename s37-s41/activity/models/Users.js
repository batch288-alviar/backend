const mongoose = require('mongoose');

// Schema
const userSchema = new mongoose.Schema({
	firstName:{
		type: String,
		required: [true, "First name is required!"]
	},

	lastName:{
		type: String,
		required: [true, "Last name is required!"]
	},

	email:{
		type: String,
		required: [true, "Email is required!"]
	},

	password:{
		type: String,
		required: [true, "Password is required!"]
	},

	isAdmin:{
		type: Boolean,
		default: false
	},

	mobileNo:{
		type: String,
		required: [true, "Mobile Number is required!"]
	},

	enrollments:[
			{
				courseId:{
					type: String,
					required: [true, "Course ID of the course is required"]
				},

				enrolledOn:{
					type: Date,
					default: new Date()
				},

				status:{
					type: String,
				default: "Enrolled"
				}
			}

		]
});

// Model

const Users = mongoose.model("Users", userSchema);

module.exports = Users;