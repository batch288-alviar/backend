// console.log("Good afternoon, Batch 288!");

// Array Methods
	// JavaScript has built-in functions and methods

	// Mutator Methods
	// Mutators methods are functions that "mutate" or change an array after they're created
	// These methods manipulate the original array performing various such as adding or removing elements.

	let fruits = ["Apple" , "Orange", "Kiwi", "Dragon Fruit"];

	// push()

	/*
		-Adds an element in the end of an array and returns the updated array's length
		Syntax:
			arrayName.push();
	*/

	console.log("Current array: ")
	console.log(fruits);

	// Outputs array length after adding
	let fruitsLength = fruits.push("Mango");

	console.log(fruitsLength)
	console.log("Mutated array from push method: ")
	console.log(fruits)

	// Push multiple elements to an array

	fruits.push("Avocado", "Guava");
	console.log("Mutated array after pushing multiple elements:");
	console.log(fruits);

	// pop()

	/*
		-removes the last element and returns the removed element
		Syntax:
			arrayName.pop();
	*/

	console.log("Current Array: ");
	console.log(fruits);

	// Outputs removed element
	let removedFruit = fruits.pop();

	console.log(removedFruit)
	console.log("Mutated array from the pop method: ")
	console.log(fruits)

	// unshift()
	/*
		-it adds one or more elements at the beginning of an array AND it returns the updated array length.
		Syntax:
			arrayname.unshift("elementA");
			arrayname.unshift("elementA", "elementB") ... ;
	*/

	console.log("Current Array: ");
	console.log(fruits);


	fruitsLength = fruits.unshift("Lime", "Banana");
	console.log("Mutated array from unshift method: ");
	console.log(fruits);

	// shift()
	/*
		-removes an element at the beginning of an array AND returns the removed element
		Syntax:
			arrayName.shift()
	*/

	console.log("Current Array: ");
	console.log(fruits);

	removedFruit = fruits.shift();

	console.log(removedFruit);
	console.log("Mutated array from the shift method: ");
	console.log(fruits);


	// splice()

	/*
		-Simultaneously removes an element from a specified index number AND (optional) adds a new element
		-Syntax:
			arrayname.splice(startingIndex, deleteConut, elementsToBeAdded)
	*/

	console.log("Current Array: ");
	console.log(fruits);

	fruits.splice(fruits.length, 0, "Cherry");
	console.log("Mutated array from the splice method: ");
	console.log(fruits);

	// sort()
	/*
		-Rearranges the array elements in alphanumeric order
		Syntax:
			arrayName.sort();
	*/

	console.log("Current Array: ");
	console.log(fruits);

	fruits.sort();

	console.log("Mutated array from the sort method: ");
	console.log(fruits);

	// reverse()
		/*
			-reverses the order of array elements
			Syntax:
				arrayName.reverse();
		*/

	console.log("Current Array: ");
	console.log(fruits);

	fruits.reverse();

	console.log("Mutated array from the reverse method: ");
	console.log(fruits);

// [Section] Non-mutator methods
/*
	-Non-mutator methods are functions that do not modify or change an array after they're created.
	-These methods do not manipulatye the original array performing various tasks such as returning elements from an array and combinig arrays and printing the outpu.
*/

	let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

	// indexOf()
	// Returns the index number of the first matching element found in an array.
	// if no match was found , the ersult will be -1.
	// The search process will be done from the first element proceeding to the last element
	/*
		syntax:
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, StartingIndex);
	*/

	let firstIndex = countries.indexOf("PH");
	console.log(firstIndex)

	let invalidCountry = countries.indexOf("BR");
	console.log(invalidCountry);

	firstIndex = countries.indexOf("PH", 2)
	console.log(firstIndex)

	// lastIndexOf()
	/*
		-returns the index number of the last matching element found in an array.
		- the search process will be done from last element proceeding to the first element.
			Syntax:
				arrayName.lastIndexOf(searchValue);
				arrayName.lastIndexOf(searchValue, StartingIndex);
	*/

	let lastIndex = countries.lastIndexOf("PH");
	console.log(lastIndex);

	invalidCountry = countries.lastIndexOf("BR");
	console.log(invalidCountry)

	lastIndexOf = countries.lastIndexOf("PH", 4)
	console.log(lastIndexOf)

	// indexOf, Starting from the starting index going to the last element(from left to right)
	// lastIndexOf, starting from the starting index going to the first element(from right to left);

	// slice()
		/*
			-portions/slices elements from an array AND returns a new array
			-syntax:
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex, endingIndex);
		*/

	let slicedArrayA = countries.slice(2);
	console.log("Result from slice method: ");
	console.log(slicedArrayA);

	// Slicing off elements from a specified index to another index:
	// The elements that will be sliced are elements from the starting index until the element before the ending index.
	let slicedArrayB = countries.slice(2, 7)
	console.log("Result from the slice method: ");
	console.log(slicedArrayB);

	// slicing off elements starting from the last elements of an array:
	let slicedArrayC = countries.slice(-3);
	console.log("Result from the slice method: ");
	console.log(slicedArrayC);

	// toString()
	/*
		returns an array as string separated by commas.
		Syntax:
			arrayName.toString
	*/

	let stringArray = countries.toString();
	console.log("Result from toString method:");
	console.log(stringArray);

	console.log(typeof stringArray);

	// concat()
	// combines arrays to an array or elements and returns the combined result.
	// Syntax:
		// arrayA.concat(arrayB);
		// arrayA.concat(elementA);

	let tasksArrayA = ["drink HTML" , "eat javascript"];
	let tasksArrayB = ["inhale CSS" , "breathe sass"];
	let tasksArrayC = ["get git", "be node"];

	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log(tasks);

	let combinedTasks = tasksArrayA.concat("smell express", "throw react");

	console.log(combinedTasks);

	// concat multiple array into an array

	let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
	console.log(allTasks);

	// concat array to an array and element
	let exampleTasks = tasksArrayA.concat(tasksArrayB, "smell express");
	console.log(exampleTasks);

	// join()
	// Returns an array as string separated by specified separator string
	// Syntax:
		// arrayName.join("separatorString")

	let users = ["John", "Jane", "Joe", "Robert"];

	console.log(users.join());
	console.log(users.join(""));
	console.log(users.join(" - "))


// [Section] Iteration Methods
	// Iteration methods are loops designed to perform repetitive tasks
	// Iteration methods loops over all items in an array

	// forEach()
	// Similar to a for loop that iterates on each of the array elements
	// Syntax:
		// arrayName.forEach(function(indivElement){statement};)

	console.log(allTasks);
	// ['drink HTML', 'eat javascript', 'inhale CSS', 'breathe sass', 'get git', 'be node']

	allTasks.forEach(function(task){
		console.log(task);
	})

	// filteredTask variable will hold all the elements from the allTasks array that has more than 10 characters
	let filteredTasks = [];

	allTasks.forEach(function(task){
		if(task.length > 10){
			filteredTasks.push(task);
		}
	});
	console.log(filteredTasks)

	// map()
	// Iterates on each element and returns new array with different values depending on the result of the function's operation/

	let numbers = [1,2,3,4,5];

	let numberMap = numbers.map(function(number){
		return number + 1;
	})
	console.log(numbers);
	console.log(numberMap);

	// every()
	/*
		it will check if all elements in an array meet the given condition
		-return true value if all elements meet the condition and false otherwse
		Syntax:
			let/const resultArray = arrayName.every(function(indivElement){
				return experssion/condition;
			})
	*/

	numbers = [1, 2, 3, 4, 5];

	let allValid = numbers.every(function(number){
		return (number<6);
	})

	console.log(allValid);

	// some()
		// checks if at least one element in the array meets the given condition.
	/*
		syntax:
		let/const resultArray = arrayName.some(function(indivElement){
			return expression/condition;
		})
	*/

	let someValid = numbers.some(function(number){
		return (number < 2);
	})

	console.log(someValid);

	// filter()
	// returns new array that contains elements which meets the give
	// returns an empty array if no elements were found
	/*
		Syntax:
			let/const resultArray = arrayName.filter(function(indivElement){
				return expression/condition;
			})
	*/

	numbers = [1, 2, 3, 4, 5]

	let filterValid = numbers.filter(function(number){
		return (number % 2 === 0);
	})

	console.log(filterValid);

	// includes()
	// checks if the argument passed can be found in the array

	let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

	let productFound1 = products.includes("Mouse");

	console.log(productFound1)

	// reduce()
	// evaluates elements from left to right and returns/reduces the array into single value

	numbers = [1,2,3,4,5];
	// The first parameter in the function will be the accumulator
	// The second parameter will be the currentValue
	let reducedArray = numbers.reduce(function(x, y){
		console.log("Accumulator: " + x);
		console.log("currentValue: " + y);
		return x+y;
	})

	console.log(reducedArray)

	let sales = [190, 12387, 12381283, 1298312, 1928312];