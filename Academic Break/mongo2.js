db.fruits.aggregate([
		{ $match : { onSale: true}},
		{ $group: {
			_id : "#origin",
			$match : { "origin": {"#Philippines"}},
			max : {$max : "$price"}
		}}
	])


db.fruits.aggregate([
		{$unwind : "$origin"},
		{$group : {
			_id : "$origin",
			kinds: { $sum: 1}
		}}
	])
