db.fruits.aggregate([
				{ $match : { onSale: true}},
				{ $group: {
					_id : "$onSale",
					avgStock: {
						$avg: "$stock"
					}
				}}
			])

db.fruits.aggregate([
				{ $unwind : "#origin"},
				{ $group: {
					_id : "#origin",
					avgStock: {
						$avg: "$stock"
					}
				}}
			])

