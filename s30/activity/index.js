async function fruitsOnSale(db) {
	return await(

			db.fruits.aggregate([
				{ $match : { onSale: true}},
				{ $group: {
					_id : "$onSale",
					NumberofFruitsOnSale: {
						$count: {}
					}
				}}
			])
		);
};



async function fruitsInStock(db) {
	return await(

			db.fruits.aggregate([
				{ $match : { stock: {$gte: 20}}},
				{ $group: {
					_id : "$supplier_id",
					fruitsWithStockMoreThan20: {
						$count: {}
					}
				}}
			])
		);
};


async function fruitsAvePrice(db) {
	return await(

			db.fruits.aggregate([
				{ $match : { onSale: true}},
				{ $group: {
					_id : "$onSale",
					avgPrice: {
						$avg: "$price"
					}
				}}
			])
		);
};


async function fruitsHighPrice(db) {
	return await(

			db.fruits.aggregate([
					{$unwind : "$origin"},
					{$group : {
						_id : "$origin",
						max : {$max : "$price"}
					}}
				])
		);
};



async function fruitsLowPrice(db) {
	return await(

			db.fruits.aggregate([
					{$unwind : "$origin"},
					{$group : {
						_id : "$origin",
						min : {$min : "$price"}
					}}
				])
		);
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};
