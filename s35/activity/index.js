const express = require('express');
// Mongoose is a package that allows creation of Schemas to Model our data structures
//Also has access to a number of methods for manipulating our database
const mongoose = require('mongoose');


const port = 3001;

const app = express();

	//Section: MongoDB Connection
	//Cpmmect to the database by passing your connection string
	// Due to update in Mongo DB drivers that allow connection, the default connection string is being flagged as an error
	//by default a warning will be displaye in the terminal when the applciation is running.
	//{newUrlParser: true}

	//Syntax: 
	//mongoose.connect("MongoDB string", {useNewUrlParser:true});

	mongoose.connect("mongodb+srv://admin:admin@batch288alviar.iizabv5.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});

	//Notification whether the we connected properly with the database.

	let db = mongoose.connection;

	//for catching the error just in case we had an error during the connection
	//console.error.bind allows us to print errors in the browser and in ther terminal
	db.on("error", console.error.bind(console, "Error! Can't connect to the Database"));

	//if the conenction is successful:
	db.once("open", ()=> console.log("We're connected to the cloud database!"));


	//Middlewares
	// Allows our app to read json data
	app.use(express.json());
	//allows our app to read data from froms
	app.use(express.urlencoded({extended:true}))


	//[Section] Mongoose Schemas
	//Schemas determine the structure of the document to be writtern in the database
	//Schemas act as blueprint to our data
	//Use the Schema() constructor of the mongoose module to create a new Schema object.

	const taskSchema = new mongoose.Schema({
		//Define the fields wuth the corresponding data type
		name: String,
		//let us another field which is status
		status: {
			type: String,
			default: "pending"
		}
	})


	//[Section] Models
	//Uses schema and are used to create/instantiate objects that correspons to the schema.
	//Models use Schema and they act as the middleman from the server(JS code) to our database.

	// To create a model we are going to use the model();

	const Task = mongoose.model('Task', taskSchema)

	//Section: Routes

	//Create a POST route to create a new task
	//Create a new task
	//Business Logic:
		// 1. Add a functionality to check whether there are duplicate tasks
			//if the task is existing in the database, we return an error
			//if the task doest exist in the database, we add it in the database
		//2. The task data will be coming from the request's body

	app.post("/tasks", (request, response) => {
		//"findOne" method is a mongoose method that acts similar to "find" in  MongoDB.
		//if the findOne finds a document that matches the criteria it will return the object/document and if there's no object that matches the criteria it will return an empty object or null.
		console.log(request.body);
		Task.findOne({name: request.body.name})
		.then(result => {
			//we can use if statement to check or verify whethere we have an object found.
			if(result !== null){
				return response.send("Duplicate task found!");
			}else{

				//Create a new task and save it to the data base.
				let newTask = new Task({
					name: request.body.name
				})

				//The save() method will store the information to the database
				//since the newTask was created from the Mongoose Schema and Task Model, it will be save in tasks collection 
				newTask.save()

				return response.send('New task creted!')
			}
		})
	})


	//Get all the tasks in our collection
	//1. Retrieve all the documents
	//2. if an error is encountered, print the error
	//3. if no error/s is/are found, send a success stuaus to the client and show the documents retrived


	app.get("/tasks", (request, response) => {
		//find method is a mongoose method that is similar to MongoDb find
		Task.find({})
		.then(result =>{
			return response.send(result);
		})
		.catch(error => response.send(error));

	})


	const userSchema = new mongoose.Schema({
		username: String,
		password: String
	})

	const User = mongoose.model('User', userSchema)

	app.post("/signup", (request, response) => {
		console.log()
		User.findOne({username: request.body.username})
		.then(result => {
			if(result !== null){
				return response.send('Duplicate username found')
			}else{
				if(request.body.username !== "" || request.body.password !== ""){
					let newUser = new User({
						username: request.body.username,
						password: request.body.password
					})

					newUser.save()
					.catch(error => response.send(error));
					
					return response.status(201).send('New user registered');


				}else{
					return response.send('BOTH username and password must be provided')
				}
			}
		})
	})




if(require.main === module){
	app.listen(port, ()=> {
		console.log(`Server is running at port ${port}!`)
	})	
}

module.exports = app;


